package hello;

// on line change

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

//Testing ground
// Starting class (3-4)
//test comment <3

public class TestGreeter {

   private Greeter g = new Greeter();

   @Test
   @DisplayName("Test for Empty Name")
   public void testGreeterEmpty() 

   {
      assertEquals(g.getName(),"");
      assertEquals(g.sayHello(),"Hello!");
   }

   @Test
   @DisplayName("Test for Ronaldo")
   public void testGreeterTed() 

   {
      g.setName("Ronaldo");
      assertEquals(g.getName(),"Ronaldo");
      assertEquals(g.sayHello(),"Hello Ronaldo!");
   }

//---------------------------------Galo_A14 Section---------------
   @Test
   @DisplayName("assertFalse Galo_A14")
   public void testFalse()
   {
      assertFalse(false, "Test execution");
   }
//----------------------------------------------------------------

   @Test
   @DisplayName("Test for Name='World'")
   public void testGreeter() 
   {

      g.setName("World");
      assertEquals(g.getName(),"World");
      assertEquals(g.sayHello(),"Hello World!");
   }

    @Test
    @DisplayName("Test for aba63_A14")
    public void aba63test() {
        g.setName("Brandon");
        double expected = 3398;
        double actual = 3398;
        assertEquals(expected, actual);
    }


}
